/**
 * Dichotomy method.
 * It takes a segment where is defined a minimum of f function and finds this point.
 *
 * @param {function} f       - function
 * @param {function} fdx     - function derivative
 * @param {function} fdxdx   - function second derivative
 * @param {number[]} segment - segment
 * @param {number} eps       - error
 *
 * @return {number} - Minimum point
 */
function dichotomy(f, fdx, fdxdx, segment, eps) {
    let delta = eps / 2;

    let i = 0;
    while (Math.abs(segment[1] - segment[0]) >= 2 * eps) {
        let x1 = (segment[0] + segment[1] - delta) / 2;
        let x2 = (segment[0] + segment[1] + delta) / 2;

        if (f(x1) <= f(x2)) {
            segment[1] = x2;
        } else {
            segment[0] = x1;
        }

        i++;
    }

    let x = (segment[0] + segment[1]) / 2;
    print_information(f, fdx, fdxdx, x, i);

    return x;
}

/**
 * Golden section search method.
 * It takes a segment where is defined a minimum of f function and finds this point (analog dichotomy).
 *
 * @param {function} f       - function
 * @param {function} fdx     - function derivative
 * @param {function} fdxdx   - function second derivative
 * @param {number[]} segment - segment
 * @param {number} eps       - error
 *
 * @return {number} - Minimum point
 */
function golden_section(f, fdx, fdxdx, segment, eps) {
    let gr = ((Math.sqrt(5) - 1) / 2); // golden ration proportion (GR)

    let i = 0;
    while (Math.abs(segment[1] - segment[0]) >= eps) {
        let d = gr * (segment[1] - segment[0]); // GR * (b - a)
        let x1 = segment[1] - d;
        let x2 = segment[0] + d;

        if (f(x1) >= f(x2)) {
            segment[0] = x1;
        } else {
            segment[1] = x2;
        }

        i++;
    }

    let x = (segment[0] + segment[1]) / 2;
    print_information(f, fdx, fdxdx, x, i);

    return x;
}