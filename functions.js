/**
 * Function f(x).
 *
 * @param {number} u - Point
 *
 * @return {number} - Function value
 */
function first_function(u) {
    let p = 4;
    let s = 3.3;
    let d = 0.07;

    return s * Math.pow(u, 4) + p / (d + u * u);
}

/**
 * Function f'(x).
 *
 * @param {number} u - Point
 *
 * @return {number} - Function value
 */
function first_function_derivative(u) {
    return 13.2 * Math.pow(u, 3) - ((8 * u) / Math.pow((u * u + 0.07), 2));
}

/**
 * Function f''(x).
 *
 * @param {number} u - Point
 *
 * @return {number} - Function value
 */
function first_function_second_derivative(u) {
    return (39.6 * u * u) + ((32 * u * u) / (Math.pow((u * u + 0.07), 3))) - (8 / Math.pow((u * u + 0.07), 2));
}

/**
 * Function f(x).
 *
 * @param {number} u - Point
 *
 * @return {number} - Function value
 */
function second_function(u) {
    return 1.9 * Math.pow(Math.sin(u), 3) + 1.1 * Math.pow(Math.cos(u), 3);
}

/**
 * Function f'(x).
 *
 * @param {number} u - Point
 *
 * @return {number} - Function value
 */
function second_function_derivative(u) {
    return 5.7 * Math.cos(u) * Math.pow(Math.sin(u), 2) - 3.3 * Math.pow(Math.cos(u), 2) * Math.sin(u);
}

/**
 * Function f''(x).
 *
 * @param {number} u - Point
 *
 * @return {number} - Function value
 */
function second_function_second_derivative(u) {
    return 6.6 * Math.cos(u) * Math.pow(Math.sin(u), 2) + 11.4 * Math.pow(Math.cos(u), 2) * Math.sin(u) - 3.3 * Math.pow(Math.cos(u), 3);
}

/**
 * Gets value of function
 *
 * @param {array} point
 *
 * @return {number}
 */
function third_function(point) {
    let x = point[0];
    let y = point[1];

    let a = 14;
    let b = -1;
    let c = 1.69;
    let d = 0.24;

    return a * x + b * y + Math.exp(c * x * x + d * y * y);
}

/**
 * Gets first partial derivative by x
 *
 * @param {array} point
 *
 * @return {number}
 */
function third_function_partial_derivative_x(point) {
    let x = point[0];
    let y = point[1];

    return 3.38 * x * Math.exp(1.69 * x * x + 0.24 * y * y) + 14;
}

/**
 * Gets first partial derivative by y
 *
 * @param {array} point
 *
 * @return {number}
 */
function third_function_partial_derivative_y(point) {
    let x = point[0];
    let y = point[1];

    return 0.48 * y * Math.exp(1.69 * x * x + 0.24 * y * y) - 1;
}

/**
 * Equation of tangent.
 *
 * @param {function} f   - function
 * @param {function} fdx - function derivative
 * @param {number} a     - part of segment
 * @param {number} b     - part of segment
 *
 * @return {number} - Function value
 */
function tangent_equation(f, fdx, a, b) {
    return (f(a) - f(b) - a * fdx(a) + b * fdx(b)) / (fdx(b) - fdx(a));
}

/**
 * Gets gradient by first partial derivative by x and y
 *
 * @param {number} dfdxValue
 * @param {number} dfdyValue
 *
 * @return {number}
 */
function grad(dfdxValue, dfdyValue) {
    return Math.sqrt(Math.pow(dfdxValue, 2) + Math.pow(dfdyValue, 2));
}

/**
 * Print information function
 *
 * @param {function} f    - function
 * @param {number} dfdxValue - derivative by x function
 * @param {number} dfdyValue - derivative by y function
 * @param {number} gradValue - gradient function
 * @param {array} point   - point
 * @param {number} i      - iterator
 *
 * @return void
 */
function print_grad_information(f, dfdxValue, dfdyValue, gradValue, point, i) {
    console.log('Minimum x: ' + point[0] + ', ' + point[1]);
    console.log('Minimum f(x): ' + f(point));
    console.log('Derivative by x f\'(x, y): ' + dfdxValue);
    console.log('Derivative by y f\'(x, y): ' + dfdyValue);
    console.log('Gradient f(x, y): ' + gradValue);
    console.log('Iteration count: ' + i);
}

/**
 * Print information function
 *
 * @param {function} f     - function
 * @param {function} fdx   - function derivative
 * @param {function} fdxdx - function second derivative
 * @param {number} x       - value
 * @param {number} i       - iterator
 *
 * @return void
 */
function print_information(f, fdx, fdxdx, x, i) {
    console.log('Minimum x: ' + x);
    console.log('Minimum f(x): ' + f(x));
    console.log('First derivative f\'(x): ' + fdx(x));
    console.log('Second derivative f\'\'(x): ' + fdxdx(x));
    console.log('Iteration count: ' + i);
}