/**
 * Step descent gradient descent method
 *
 * @param {function} f
 * @param {function} dfdx
 * @param {function} dfdy
 * @param {function} grad
 * @param {array}    point
 * @param {number}   stepLength
 * @param {number}   eps
 *
 * @return {array}
 */
function gradient_descent(f, dfdx, dfdy, grad, point, stepLength, eps) {
    if (point.length < 2) {
        throw new DOMException('Point length cannot be less that 2');
    }

    let i = 0;
    let gradValue = 9999; //just for init
    let dfdxValue = dfdx(point);
    let dfdyValue = dfdy(point);

    while (gradValue > eps) {
        let a = 0;
        let b = 10;
        let l = b - a;

        while (l >= 2 * eps) {
            let u1 = (a + b - eps / 2) / 2;
            let u2 = (a + b + eps / 2) / 2;

            //just function is not from x, but is from x - u1 * dfdx or dfdy
            let g1 = f([point[0] - u1 * dfdxValue, point[1] - u1 * dfdyValue,]);
            let g2 = f([point[0] - u2 * dfdxValue, point[1] - u2 * dfdyValue,]);

            if (g1 <= g2) {
                b = u2;
            } else {
                a = u1;
            }

            l = b - a;
        }

        stepLength = (a + b) / 2;
        point = [
            point[0] - stepLength * dfdxValue,
            point[1] - stepLength * dfdyValue,
        ];

        dfdxValue = dfdx(point);
        dfdyValue = dfdy(point);

        gradValue = grad(dfdxValue, dfdyValue);

        i++;
    }

    print_grad_information(f, dfdxValue, dfdyValue, gradValue, point, i);

    return point;
}