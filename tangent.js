/**
 * Tangent method.
 *
 * @param {function} f       - function
 * @param {function} fdx     - function derivative
 * @param {function} fdxdx   - function second derivative
 * @param {number[]} segment - segment
 * @param {number} eps       - error
 *
 * @return {number} - Minimum point
 */
function tangent(f, fdx, fdxdx, segment, eps) {
    let a = segment[0];
    let b = segment[1];

    let i = 0;
    while (Math.abs(b - a) >= eps) {
        let x1 = tangent_equation(f, fdx, a, b);

        if (fdx(x1) >= 0) {
            b = x1;
        } else {
            a = x1;
        }

        i++;
    }

    let x = (a + b) / 2;
    print_information(f, fdx, fdxdx, x, i);

    return x;
}